drop table if exists group_b;
drop table if exists group_a;
drop table if exists fact_table;
drop table if exists temp_gb;
drop table if exists temp_ga;

CREATE TABLE group_a (
	group_a_id int NOT NULL,
	code_a varchar(32) NOT NULL,
	processed_dttm timestamp not NULL default current_timestamp,
	week_num int null default WEEK(processed_dttm),
	year_num int null default YEAR(processed_dttm),
	CONSTRAINT group_a_pk PRIMARY KEY (group_a_id,year_num,week_num)
) 
PARTITION BY HASH (year_num*100 + week_num)
PARTITIONS 100;

CREATE TABLE group_b (
	group_b_id int NOT NULL,
	code_b varchar(32) NOT NULL,
	processed_dttm timestamp not NULL default current_timestamp,
	week_num int NULL default WEEK(processed_dttm),
	year_num int NULL default YEAR(processed_dttm),
	CONSTRAINT group_b_pk PRIMARY KEY (group_b_id,year_num,week_num)
) 
PARTITION BY HASH (year_num*100 + week_num)
PARTITIONS 100;

CREATE TABLE fact_table (
	fact_id int NOT NULL,
	fact_message varchar(32) NOT NULL,
	group_a_id int NOT NULL,
	group_b_id int NOT NULL,
	processed_dttm timestamp not NULL default current_timestamp,
	week_num int NULL default WEEK(processed_dttm),
	year_num int NULL default YEAR(processed_dttm),
	CONSTRAINT fact_table_pk PRIMARY KEY (fact_id,year_num,week_num)
)
PARTITION BY HASH (year_num*100 + week_num)
PARTITIONS 100;

CREATE TABLE temp_ga (
	group_a_id int NOT NULL,
	code_a varchar(32) NOT NULL,
	processed_dttm timestamp NOT NULL,
	CONSTRAINT temp_ga_pk PRIMARY KEY (group_a_id)
);

CREATE TABLE temp_gb (
	group_b_id int NOT NULL,
	code_b varchar(32) NOT NULL,
	processed_dttm timestamp NOT NULL,
	CONSTRAINT temp_gb_pk PRIMARY KEY (group_b_id)
);