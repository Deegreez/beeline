drop table if exists public.fact_table;
drop table if exists public.group_a;
drop table if exists public.group_b;



create table public.group_a (
   group_a_id int NOT null,
   code_a varchar(32) not null,
   processed_dttm timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
   constraint group_a_pk PRIMARY key (group_a_id)
);

create table public.group_b (
   group_b_id int NOT null,
   code_b varchar(32) not null,
   processed_dttm timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
   constraint group_b_pk PRIMARY key (group_b_id)
);

create table public.fact_table (
   fact_id int NOT NULL,
   fact_message varchar(32) not null,
   group_a_id int not null,
   group_b_id int not null,
   processed_dttm timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
   constraint fact_table_pk PRIMARY key (fact_id),
   CONSTRAINT fact_table_group_a_id_fkey FOREIGN KEY (group_a_id) REFERENCES group_a(group_a_id) ON DELETE cascade,
   CONSTRAINT fact_table_group_b_id_fkey FOREIGN KEY (group_b_id) REFERENCES group_b(group_b_id) ON DELETE cascade
);

---------------------------------------------------------------------
insert into public.group_a(group_a_id,code_a)
select
    i as group_a_id,
    left(md5(random()::text), ceil(random() * 32)::int) as code_a
from generate_series(1,150) s(i);

insert into public.group_b(group_b_id,code_b)
select
    i as group_b_id,
    left(md5(random()::text), ceil(random() * 32)::int) as code_b
from generate_series(1,300) s(i);

insert into public.fact_table (fact_id,fact_message,group_a_id,group_b_id)
select 
	 i as fact_id
	,left(md5(random()::text), ceil(random() * 32)::int) as fact_message
	,ceil(random() * 150)::int as group_a_id
	,ceil(random() * 300)::int as group_b_id
from generate_series(1,10000) s(i)
;